﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUIState : IState 
{

    public void Setup()
    {
        ChatManager.s_Instance.ShowMainMenuUI();
    }

    public void DoUpdate()
    {
        
    }

    public void TearDown()
    {
        
    }
     
}
