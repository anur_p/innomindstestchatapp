﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatUI : MonoBehaviour 
{

    [SerializeField]
    Button m_SendButton;
   
    [SerializeField]
    Button m_BackButton;

    [SerializeField]
    InputField m_MessageText;

    [SerializeField]
    Text m_UserNameText;

    [SerializeField]
    Transform m_ChatScrollList;

    [SerializeField]
    GameObject m_ChatBubble;

    [SerializeField]
    GameObject m_ChatBubbleOther;

    List<string> m_Messages;

    public string m_UserName;


    // Use this for initialization
    void Start () 
    {
        m_SendButton.onClick.AddListener(OnMessageSend);
        m_BackButton.onClick.AddListener(OnBackButtonClicked);

    }

    public void Activate()
    {
        Dictionary<string, List<ChatManager.Message>> allChatConversations = ChatManager.s_Instance.allChatConversations;

        if(allChatConversations.ContainsKey(m_UserName))
        {
            List<ChatManager.Message> conversation = allChatConversations[m_UserName];
            foreach(var message in conversation)
            {
                CreateChatBubble(message.m_Message, message.m_MessageType);
            }
        }
    }
       

    #if UNITY_EDITOR
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            OnMessageSend();
        }
    }
    #endif

    void OnMessageSend()
    {

        string message = m_MessageText.text;

        if(string.IsNullOrEmpty(message))
            return;


//        GameObject chatBubble = (GameObject)GameObject.Instantiate(m_ChatBubble);
//
//        chatBubble.GetComponent<ChatBubble>().SetBubbleText(message);
//
//        chatBubble.transform.parent = m_ChatScrollList.transform;


        CreateChatBubble(message, ChatManager.MessageType.SELF);

        ChatManager.s_Instance.SendChatMessage(m_UserName, message);
       

        Dictionary<string, List<ChatManager.Message>> allChatConversations = ChatManager.s_Instance.allChatConversations;
        List<ChatManager.Message> conversation = null;

        if(allChatConversations.ContainsKey(m_UserName))
        {
            conversation = allChatConversations[m_UserName];

        }
        else 
        {
            conversation = new List<ChatManager.Message>();
            allChatConversations[m_UserName] = conversation;
        }
        conversation.Add(new  ChatManager.Message(ChatManager.MessageType.SELF, message));
        m_MessageText.text = "";
        //add message to list
    }

    public void SetUserName(string userName)
    {
        m_UserName =  userName;
        m_UserNameText.text = userName;
    }

    void OnBackButtonClicked()
    {
        for(int idx = 0; idx < m_ChatScrollList.childCount; idx++)
        {
            Transform child = m_ChatScrollList.GetChild(idx);

            GameObject.Destroy(child.gameObject);
        }

        gameObject.SetActive(false);
       
        //serialize list to json.
        //make UI invisible.
        //make previous UI visible
    }

    public void OnMessageReceived(string message)
    {
        CreateChatBubble(message, ChatManager.MessageType.OTHER);
    }

    void CreateChatBubble(string message, ChatManager.MessageType type)
    {
        GameObject goToInstantiate = m_ChatBubble;
        if(type == ChatManager.MessageType.OTHER)
            goToInstantiate = m_ChatBubbleOther;
       
        GameObject chatBubble = (GameObject)GameObject.Instantiate(goToInstantiate);

        chatBubble.GetComponent<ChatBubble>().SetBubbleText(message);

        chatBubble.transform.parent = m_ChatScrollList.transform;
    }
}
