﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState 
{
    void Setup();

    void DoUpdate();

    void TearDown();
}
