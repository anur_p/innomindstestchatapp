using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FriendUIItem : MonoBehaviour
{
    Text m_FriendNameText;
    Button m_FriendButton;
    string m_FriendID;

   
    void Awake()
    {
        m_FriendNameText = GetComponent<Text>();
        m_FriendButton = GetComponent<Button>();

        m_FriendButton.onClick.AddListener(OnFriendClicked);
    }

    public void SetFriendName(string name)
    {
        SetFriendNameText(name);

        SetFriendId(name);
     }

    public void SetFriendId(string Id)
    {
        m_FriendID = Id;
    }

    public void SetFriendNameText(string name)
    {
        m_FriendNameText.text = name;
    }

    void OnFriendClicked()
    {
        MainUIManager.s_Instance.ShowChat(m_FriendID);
    }
        
}

