﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUIManager : MonoBehaviour 
{

    [SerializeField]
    GameObject m_FriendsMenu;

    [SerializeField]
    GameObject m_ChatMenu;

    [SerializeField]
    Button m_FriendsMenuBtn;


    [SerializeField]
    Button m_ChatMenuBtn;

    [SerializeField]
    GameObject m_ChatUI;

    public static MainUIManager s_Instance = null;

    ChatUI m_ChatUIScript;


   
	// Use this for initialization
	void Start () 
    {
        m_FriendsMenuBtn.onClick.AddListener(OnFriendsBtnClicked);
        m_ChatMenuBtn.onClick.AddListener(OnChatsBtnlicked);

        if(s_Instance != null)
            Destroy(this);
        else
           s_Instance = this;

        m_ChatUIScript = m_ChatUI.GetComponent<ChatUI>();

        ChatManager.s_Instance.OnMessageReceived = OnMessageReceived;


	}
	
    void OnFriendsBtnClicked()
    {
        m_FriendsMenu.SetActive(true);
        m_ChatMenu.SetActive(false);
        m_FriendsMenuBtn.enabled = false;
        m_ChatMenuBtn.enabled = true;
    }

    void OnChatsBtnlicked()
    {
        m_FriendsMenu.SetActive(false);
        m_ChatMenu.SetActive(true);

        m_FriendsMenuBtn.enabled = true;
        m_ChatMenuBtn.enabled = false;

        var chatMenuComponent = m_ChatMenu.GetComponentInChildren<ChatsMenu>();
        chatMenuComponent.Activate();
    }

    public void ShowChat(string friendID)
    {
        m_ChatUI.SetActive(true);

        m_ChatUI.GetComponent<ChatUI>().SetUserName(friendID);
        m_ChatUI.GetComponent<ChatUI>().Activate();

    }

    void OnMessageReceived(string userID, string message)
    {
        if(m_ChatUI.activeInHierarchy && m_ChatUIScript.m_UserName == userID)
        {
            m_ChatUIScript.OnMessageReceived(message);
        }
    }
}
