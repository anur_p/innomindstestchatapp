﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon.Chat;
using System;

public class ChatManager : MonoBehaviour, IChatClientListener
{
    public class Message
    {
        public MessageType m_MessageType;
        public string m_Message;
        public Message(MessageType type, string message)
        {
            m_MessageType = type;
            m_Message = message;
        }
    }

    public enum MessageType
    {
        SELF,
        OTHER
    }

    [SerializeField]
    GameObject m_LoginUI;
    [SerializeField]
    GameObject m_MainUI;
    [SerializeField]
    GameObject m_ChatBox;

    IState m_CurrentState;

    ChatClient m_Client;

    Action m_OnLoggedInCallback;

    public Action<string, string> OnMessageReceived;

    public Action<String> m_OnInfoEntered;

    bool m_IsLoggedIn;

    public static ChatManager s_Instance {get; private set;}

    public string SelfUserID {get {return m_Client.UserId;}}

    public Dictionary<string, List<Message>> allChatConversations = new Dictionary<string, List<Message>>();

	// Use this for initialization
	void Start () 
    {
        if(s_Instance != null)
            Destroy(this);
        else
            s_Instance = this;

        m_Client = new ChatClient(this);

        SetState(new LoginState());
	}
	
	// Update is called once per frame
	void Update () 
    {
        
        if(m_CurrentState != null)
            m_CurrentState.DoUpdate();

        if(m_Client != null)
            m_Client.Service();
	}
        

    public void SetState(IState state)
    {
        if(m_CurrentState != null)
            m_CurrentState.TearDown();

        m_CurrentState = state;

        m_CurrentState.Setup();
    }


    public void Login(string userId, System.Action onLoggedIn)
    {
        m_Client.Connect(ChatSettings.Instance.AppId, "1.0", new AuthenticationValues(userId));
        m_OnLoggedInCallback = onLoggedIn;
    }


    public void ShowLoginUI(Action<String> onInfoEntered)
    {
        m_LoginUI.SetActive(true);
        m_OnInfoEntered = onInfoEntered;
    }

    public void ShowMainMenuUI()
    {
        m_MainUI.SetActive(true);
    }

    public void HideLoginUI()
    {
        m_LoginUI.SetActive(false);
    }
       

    public void AddFriend(string userID)
    {
        m_Client.AddFriends(new[]{userID});
    }

    public void SendChatMessage(string userID, string message)
    {
        m_Client.SendPrivateMessage(userID, message);
    }

    public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message)
    {
        //throw new System.NotImplementedException();
        Debug.Log("Coming here message = "+message);
    }

    public void OnDisconnected()
    {
        //  throw new System.NotImplementedException();
    }

    public void OnConnected()
    {
        Debug.Log("Connected");
        if(m_OnLoggedInCallback != null)
          m_OnLoggedInCallback();
        else
            Debug.LogError("m_OnLoggedInCallback was not set");
        // throw new System.NotImplementedException();
    }

    public void OnChatStateChange(ChatState state)
    {
        //throw new System.NotImplementedException();
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        //throw new System.NotImplementedException();
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        Debug.LogWarning("Received message from "+sender+" message = "+message.ToString() +" "+m_Client.AuthValues.UserId+" "+gameObject.name);

        List<Message> conversation = null;
        if(allChatConversations.ContainsKey(sender))
        {
            conversation = allChatConversations[sender];

        }
        else
        {
            conversation = new List<Message>();
            allChatConversations[sender] = conversation;
        }
        conversation.Add(new  Message(MessageType.OTHER, (string)message));
        OnMessageReceived(sender, (string)message);
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        // throw new System.NotImplementedException();
    }

    public void OnUnsubscribed(string[] channels)
    {
        //throw new System.NotImplementedException();
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        //throw new System.NotImplementedException();
    }
}
