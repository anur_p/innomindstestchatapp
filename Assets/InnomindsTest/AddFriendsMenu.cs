﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddFriendsMenu : MonoBehaviour 
{

    [SerializeField]
    Button m_AddFriendsButton;

    [SerializeField]
    Button m_CancelButton;

    [SerializeField]
    InputField m_UserIDText;

    public FriendsMenu m_FriendsMenu;

	// Use this for initialization
	void Start () 
    {
        m_AddFriendsButton.onClick.AddListener(OnAddFriend);
        m_CancelButton.onClick.AddListener(OnCancel);

	}
	
	
    public void OnAddFriend()
    {
        string userID = m_UserIDText.text;

        bool isInputValid = ValidateInput(userID);

       // if(isInputValid)
        //    ChatManager.s_Instance.AddFriend(userID);

        gameObject.SetActive(false);

        m_UserIDText.text = "";

        m_FriendsMenu.AddFriend(userID);
            
    }

    void OnCancel()
    {
        gameObject.SetActive(false);

        m_UserIDText.text = "";
    }

    public bool ValidateInput(string userID)
    {

        //stub function for now.
        return true;
    }
}
