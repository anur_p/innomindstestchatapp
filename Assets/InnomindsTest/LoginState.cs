﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginState : IState 
{

    public void Setup()
    {
        string userID = PlayerPrefs.GetString("UserID");

      //  if (string.IsNullOrEmpty(userID) == false)
       //     ChatManager.s_Instance.Login(userID, OnLoggedIn);
       // else
            ChatManager.s_Instance.ShowLoginUI(OnInfoEntered);

    }

    public void DoUpdate()
    {
        
    }

    public void TearDown()
    {
        ChatManager.s_Instance.HideLoginUI();
    }

    void OnInfoEntered(string userID)
    {
        ChatManager.s_Instance.Login(userID, OnLoggedIn);
        PlayerPrefs.SetString("UserID", userID);
    }

    void OnLoggedIn()
    {
        ChatManager.s_Instance.SetState(new MainUIState());
    }
}
