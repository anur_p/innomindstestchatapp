﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon.Chat;

public class MyChatScript : MonoBehaviour, IChatClientListener
{
    ChatClient client;
	// Use this for initialization
	void Start () {

        client = new ChatClient(this);
        client.Connect(ChatSettings.Instance.AppId, "Something", new AuthenticationValues("Anur"));
		
	}
	
	// Update is called once per frame
	void Update () {
        client.Service();
		
	}

    #region IChatClientListener implementation

    public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message)
    {
        //throw new System.NotImplementedException();
        Debug.Log("Coming here message = "+message);
    }

    public void OnDisconnected()
    {
        Debug.LogWarning("Disconnected");
      //  throw new System.NotImplementedException();
    }

    public void OnConnected()
    {
        Debug.Log("Connected");
       // throw new System.NotImplementedException();
    }

    public void OnChatStateChange(ChatState state)
    {
        //throw new System.NotImplementedException();
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        //throw new System.NotImplementedException();
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        Debug.LogWarning("Received message from "+sender+" message = "+message.ToString());
        //client.SendPrivateMessage("Anur1", "HI this is a test message");
       // throw new System.NotImplementedException();
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
       // throw new System.NotImplementedException();
    }

    public void OnUnsubscribed(string[] channels)
    {
        //throw new System.NotImplementedException();
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        //throw new System.NotImplementedException();
    }

    public void OnGUI()
    {
        if(GUI.Button(new Rect(10,10, 100, 100), "Send to Anur"))
        {
            client.SendPrivateMessage("Anur", "Hi this is a test message !");
        }
    }

    #endregion
}
