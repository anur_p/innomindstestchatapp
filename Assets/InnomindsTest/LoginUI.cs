﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginUI : MonoBehaviour 
{
    [SerializeField]
    Button m_Button;

    [SerializeField]
    InputField m_Login;

    [SerializeField]
    Text m_IncorrectText;

    bool m_IsClicked;
	// Use this for initialization
	void Start () 
    {
        m_Button.onClick.AddListener(OnButtonClicked);
        m_Login.onValueChange.AddListener(OnTextEdited);
	}
	

    void OnButtonClicked()
    {
        string userID = m_Login.text;

        bool isInputValid = ValidateUserID(userID);

        if(isInputValid && !m_IsClicked)
        {
            ChatManager.s_Instance.m_OnInfoEntered(userID);
            m_IsClicked = true;
        }
        else
        {
            Debug.LogError("Input in invalid");
            m_IncorrectText.gameObject.SetActive(true);
            //Add textfield to say the input is invalid
        }
    }

    void OnTextEdited(string text)
    {
        m_IncorrectText.gameObject.SetActive(false);
    }

    bool ValidateUserID(string userID)
    {

        if(string.IsNullOrEmpty(userID))
            return false;
        if(userID.Contains(" ") || userID.Contains(";") || userID.Contains("\"") || userID.Contains("\\") || userID.Contains("/") || userID.Contains("?"))
            return false;
        if(userID.Length < 6)
            return false;
        return true;
    }
	
}
