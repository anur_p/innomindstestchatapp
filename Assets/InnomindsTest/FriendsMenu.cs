﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FriendsMenu : MonoBehaviour 
{
    [SerializeField]
    AddFriendsMenu m_AddFriendsMenu;

    [SerializeField]
    Button m_AddFriendsMenuBtn;

    [SerializeField]
    GameObject m_FriendElementTemplate;

    public MainUIManager m_MainUI;


	// Use this for initialization
	void Start () 
    {
        m_AddFriendsMenuBtn.onClick.AddListener(OnAddFriendsClicked);
	}


    public void OnAddFriendsClicked()
    {
        m_AddFriendsMenu.gameObject.SetActive(true);
        m_AddFriendsMenu.m_FriendsMenu = this;
    }

    public void AddFriend(string userID)
    {
        GameObject newFriend = (GameObject)Instantiate(m_FriendElementTemplate);

        FriendUIItem newFriendItem = newFriend.GetComponent<FriendUIItem>();

        newFriendItem.SetFriendName(userID);

        newFriendItem.transform.parent = transform;
    }

}
