﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatBubble : MonoBehaviour 
{
    [SerializeField]
    Text m_BubbleText;

    public void SetBubbleText(string messageText)
    {
        m_BubbleText.text = messageText;
    }
	
}
