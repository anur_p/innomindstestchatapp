﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatsMenu : MonoBehaviour
{
    [SerializeField]
    GameObject m_FriendElementTemplate;

    public void Activate()
    {
        for(int idx = 0; idx < transform.childCount; idx++)
        {
            var child = transform.GetChild(idx);
            Destroy(child.gameObject);
        }

        Dictionary<string, List<ChatManager.Message>> allChatConversations = ChatManager.s_Instance.allChatConversations;

        foreach(string userID in allChatConversations.Keys)
        {
            if(userID != ChatManager.s_Instance.SelfUserID)
             AddChatElement(userID);
        }
    }
        
    public void AddChatElement(string userID)
    {
        GameObject newFriend = (GameObject)Instantiate(m_FriendElementTemplate);

        FriendUIItem newFriendItem = newFriend.GetComponent<FriendUIItem>();

        newFriendItem.SetFriendNameText("Chat with "+userID);

        newFriendItem.SetFriendId(userID);

        newFriendItem.transform.parent = transform;
    }
}
