This is a barebones chat application based on the photon server SDK for Unity.

It uses the photon server as the backened for sending and receiving messages.
One needs to login with any user name, add a friend with any username, tap on the friend name to start a private chat with the friend.
Photon server does not handle authentication and persist user information, so there is no real authenticaion or persistent user creation.

On logging in a menu appears which has the options to show added friends (and add friends) and current chats.
On Tapping the 'add friend text', the add friend dialog box appears. When a userid is added, it appears on the scrollable menu.

Tapping on the name opens up the chat box. When another client logs in with the user id that was added as friend, the messages 
should reach the other client and vice versa. However this functionality is not fully functional and the messages are not getting
received. Still debugging the issue.
Chat history is persisted within a session. This can be made to persist across sessions if needed, by adding some code.

Login information can also be persisted across sessions very simply by uncommenting some code in the app.

It was built on Unity 2017.4.5f1.



